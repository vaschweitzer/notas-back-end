const express = require('express');
const mongoose = require('mongoose');
const requireDir = require('require-dir');
const cors = require('cors');

// Iniciando o App
const app = express();

// configura express para aceitar recebimento de dados em JSON nas requisições
app.use(express.json());

// permite que o servidor seja acessado de fora do localhost. Possível passar prâmetros para restrições de segurança
app.use(cors());

// Iniciando o DB. Database = "nodeapi"
mongoose.connect(
    'mongodb://localhost:27017/nodeapi',
    { useNewUrlParser: true }
);

requireDir('./src/models');

/* Utiliza rotas definidas no arquivo /src/routes
  "Use" é um  coringa para receber qualquer requisião (GET, POST, etc)
  /api é a url padrão incial par aas requisições:  localhost:3001/api/
*/
app.use("/api", require("./src/routes"));

// Porta utilizada pela api pra receber requisições
app.listen(3001);