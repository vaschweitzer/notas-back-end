const mongoose = require('mongoose');
const Nota = mongoose.model("Nota");

// Necessário para o findByIdAndUpdate após uma atualização
mongoose.set('useFindAndModify', false);

module.exports = {

    // Retorna todas as notas paginadas por dez itens
    async index(req, res) {
        const { page = 1 } = req.query; 
        const notas = await Nota.paginate (
            {},
            { page, limit: 10, sort: { alteradoEm: -1 } }
        );
        return res.json(notas);
    },

    // Retorna uma nota de acordo com ID passado
    async show(req, res) {
        const nota = await Nota.findById(req.params.id);
        return res.json(nota);
    },

    // Armazena uma nova nota
    async store(req, res) {
        const nota = await Nota.create(req.body);
        return res.json(nota);
    },

    // Atualiza uma nota de acordo com o ID passado
    async update(req, res) {
        const nota = await Nota.findByIdAndUpdate(req.params.id, req.body, { new: true });    // último parâmetro faz com que seja retornada a nota APÓS a atualização, caso contrário retornaria a nota antes de atualizar.
        return res.json(nota);
    },

    // Remove uma nota de acordo com o ID passado
    async destroy(req, res) {
        await Nota.findByIdAndDelete(req.params.id);
        return res.send();
    }

};