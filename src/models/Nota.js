const mongoose = require('mongoose');
const mongooesPaginate = require('mongoose-paginate');

const NotaSchema = new mongoose.Schema({
    titulo: {
        type: String,
        required: true,
    },
    descricao: {
        type: String,
        required: true,
    },
    criadoEm: {
        type: Date,
        default: Date.now,
    },
    alteradoEm: {
        type: Date,
        default: Date.now,
    }
});

NotaSchema.plugin(mongooesPaginate);  
mongoose.model('Nota', NotaSchema);