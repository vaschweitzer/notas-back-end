const express = require('express');
const routes = express.Router();

const NotaController = require('./controllers/NotaController');

routes.get('/notas', NotaController.index);
routes.get('/notas/:id', NotaController.show);
routes.post('/notas', NotaController.store);
routes.put('/notas/:id', NotaController.update);
routes.delete('/notas/:id', NotaController.destroy);

module.exports = routes;