Projeto de back end para aplicação web de registro de tarefas

Desenvolvido em formato de servidor API REST. 
Recebe requisições HTTP e retorna dados no formato JSON para o front end.

## Requisitos
- Possuir o SGBD MongoDB e Node.js instalados.
- Link do projeto front: https://gitlab.com/vaschweitzer/notas-front-end

## Passos para execução do projeto

- Clonar o projeto GIT ou fazer o download do mesmo em sua máquina local;
- Entrar na pasta principal do projeto;
- Executar "npm install" para baixar as bibliotecas utilizadas e suas dependências;
- Executar "npm run dev";
